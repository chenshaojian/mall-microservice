package com.demo.mall.wap.controller;


import com.demo.goods.client.service.GoodsRestService;
import com.demo.goods.object.GoodsQo;
import com.demo.order.client.service.OrderRestService;
import com.demo.order.client.util.TreeMapConvert;
import com.demo.order.object.OrderDetailQo;
import com.demo.order.object.OrderQo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderRestService orderRestService;
    @Autowired
    private GoodsRestService goodsRestService;


    @RequestMapping(value="/index")
    public ModelAndView index(ModelMap model) throws Exception{
        return new ModelAndView("order/index");
    }


    @RequestMapping(value="/{id}")
    public ModelAndView findById(@PathVariable Long id) {
        String json = orderRestService.findById(id);
        OrderQo orderQo = new Gson().fromJson(json, OrderQo.class);
        return new ModelAndView("order/show", "order", orderQo);
    }


    @RequestMapping(value = "/list")
    public Page<Map<String, Object>> findAll(OrderQo orderQo) {
        String json = orderRestService.findPage(orderQo);
        Gson gson = TreeMapConvert.getGson();
        TreeMap<String,Object> page = gson.fromJson(json, new TypeToken< TreeMap<String,Object>>(){}.getType());

        Pageable pageable = PageRequest.of(orderQo.getPage(), orderQo.getSize(), null);
        List<OrderQo> list = new ArrayList<>();

        if(page != null && page.get("content") != null)
            list = gson.fromJson(page.get("content").toString(), new TypeToken<List<OrderQo>>(){}.getType());
        String count = page.get("totalelements").toString();

        return new PageImpl(list, pageable, new Long(count));
    }

    @RequestMapping(value="/verify")
    public ModelAndView verify(ModelMap model) throws Exception{
        return new ModelAndView("order/verify");
    }

    @RequestMapping(value="/switch")
    public ModelAndView toswitch(ModelMap model) throws Exception{
        return new ModelAndView("order/switch");
    }

    @RequestMapping(value="/accounts/{id}")
    public ModelAndView accounts(ModelMap model, @PathVariable Long id) {
        String json = goodsRestService.findById(id);
        GoodsQo goodsQo = new Gson().fromJson(json, GoodsQo.class);
        return new ModelAndView("order/accounts", "goods", goodsQo);
    }

    @RequestMapping(value="/buyone", method = RequestMethod.POST)
    public String buyone(OrderQo buyone) {
        String json = goodsRestService.findById(buyone.getId());
        GoodsQo goodsQo = new Gson().fromJson(json, GoodsQo.class);
        if(goodsQo != null){
            Integer sum = 1;
            OrderDetailQo orderDetailQo = new OrderDetailQo();
            orderDetailQo.setGoodsid(goodsQo.getId());
            orderDetailQo.setGoodsname(goodsQo.getName());
            orderDetailQo.setPrice(goodsQo.getPrice());
            orderDetailQo.setPhoto(goodsQo.getPhoto());
            orderDetailQo.setNums(sum);
            orderDetailQo.setMoney(sum * goodsQo.getPrice());

            OrderQo orderQo = new OrderQo();
            orderQo.addOrderDetail(orderDetailQo);
            orderQo.setUserid(buyone.getUserid());
            orderQo.setMerchantid(goodsQo.getMerchantid());
            orderQo.setAmount(sum * goodsQo.getPrice());
            orderQo.setOrderNo(new Long((new Date()).getTime()).toString());
            //待发货
            orderQo.setStatus(1);

            String sid = orderRestService.create(orderQo);

            if(sid != null && new Integer(sid) > 0) {
                Integer buynum = goodsQo.getBuynum() == null ? sum : sum + goodsQo.getBuynum();
                goodsQo.setBuynum(buynum);
                goodsRestService.update(goodsQo);
                return sid;
            }
        }
        return "-1";
    }

}
