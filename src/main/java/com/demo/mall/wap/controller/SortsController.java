package com.demo.mall.wap.controller;

import com.demo.catalog.client.service.SortsRestService;
import com.demo.catalog.client.util.TreeMapConvert;
import com.demo.catalog.object.SortsQo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/sorts")
public class SortsController {

    @Autowired
    private SortsRestService sortsRestService;


    @RequestMapping(value="/index")
    public ModelAndView findAll() {
        Gson gson = TreeMapConvert.getGson();
        List<SortsQo> sortses = gson.fromJson(sortsRestService.findList(), new TypeToken<List<SortsQo>>(){}.getType());
        return new ModelAndView("sorts/index", "sortses", sortses);
    }
}
